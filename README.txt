*Description*

User Create By Role is a very simple module that is meant to simplify creation of user accounts. All it does is add additional action links in addition to "Add user" for each role. For example, if a site had two roles: author and editor, it would add an "Add author" and an "Add editor" action link to the User Administration page.

*How To Use*
1. Enable module as normal
2. This module does nothing by default... that is, it is necessary to create and/or enable roles to show their own create links. Creating a role is outside the scope of these instructions.
3. To enable a role to display a create link shortcut, visit www.example.com/admin/config/people/user_create_by_role and check the boxes next to the roles you wish to enable this functionality for.
4. Visit the People page and you should see your links.
